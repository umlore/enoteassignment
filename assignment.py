"""

written using python version 3.7.1

1.  A list is an ordered data structure that allows for quick indexing, and allows for duplicate values to be stored. A set is an unordered data structure that requires all elements to be unique.  Both are type non-specific containers, but sets require all elements to be hashable (whereas lists do not), so you can create a list of lists, but a set of sets requires the nested sets to be converted to frozensets, which are hashable because they are immutable.  Because sets are implemented as hashtables, lookup is O(1), whereas it is O(N) for lists, which are unordered.

2.  Concurrency methods:

    atomic programming - an atomic operation is one which happens "all at once".  Because atomic operations do not result in any side effects until the sum total of all operations within it have completed, it is safe for multiple threads to operate concurrently.  Atomicity can sometimes result in complicated program logic.  It works best when the order of access of a variable for different threads is not important.  It works very well for handling server messages from many clients.

    mutex locking - a mutex is an object designed to be locked and unlocked around a "critical section", code that cannot be entered by any other process while another is executing it.  Poor lock hygiene can result in deadlock errors.  It is also inefficient for real time applications like audio signal processing, as schedulers will often invert the priority of threads due to mutex locking, causing interruptions to the sound hardware's callback loop, as priority inversion is an expensive operation.

    lock-free programming - to share state between threads running concurrently without locking, there exist "lock-free" data structures for communicating back and forth.  Algorithms check what index of a fixed size buffer each producer and consumer thread can access, and separate threads can access it simultaneously without conflict or requiring locking.  The algorithms become extremely complicated as optimizations are required for the data structure; in my experience it is most effective to pass data through as json using a simple ring buffer.  Lock-free structures are easiest with single-producer, single-consumer models, but can be theoretically extended to n-producer n-consumer models if necessary.

    distributed communication protocols - when programs are run over networks with separate machines, there are many ways of sharing information between threads and achieving concurrency.  In a distributed system, message passing between processes becomes extremely expensive in terms of real-time, because of network delay.  Messages can be sent and received in wildly different orders, and can also be easily lost.  There are different standards for data loss, UDP which is much lossier than TCP which is slower, etc.  The advantage, of course, is the much greater computational power of an entire concurrent network over a single machine.  

3.  Quicksort has an edge case resulting in O(n^2) execution if the pivot value chosen is the minimum or maximum value.  The closer to the median value a pivot is chosen, the greater efficiency of quicksort (up to O(n log(n))).  Quicksort has been improved largely by implementing better pivot choice schemes.  

4.  In the context of matrix transformations, an eigenvector is any vector that remains on its original span after a matrix transformation has been applied.  The associated eigenvalue represents how much the given eigenvector has been scaled.

"""


#5. 
#generate fibonacci list
#python 3 ints don't have a maximum value, we can tread without fear of out-of-bounds
fib = [0, 1]
for i in range(2, 100):
    fib.append(fib[-1] + fib[-2])

#reverse it!
fib = fib[::-1]
print (fib)